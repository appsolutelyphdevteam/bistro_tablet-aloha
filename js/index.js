var GLOBAL_MEMBER_ID;
var GLOBAL_VOUCHER_LIST;

$(function(){
	addEventListeners();
	getBadgeCount( 'pointtable' );
	getBadgeCount( 'registertable' );
	getBadgeCount( 'converttable' );
});

function addEventListeners(){
	FastClick.attach(document.body); // removes 300ms delay
	
	$( "#settings" ).on( 'click', settingsHandler ); // header buttons
	$( "#navigation" ).on( 'click', navigationHandler );	// footer buttons

	$(".view #logout.btn").on('click', function(){
		showModal('confirm_logout', { 
			callback: function(){ nativeInterface.done() },
			message: "Are you sure you want to log out?" 
		});
	});

	$("#modal").on('click', function(e) { if(e.target == e.currentTarget) closeModal() });

	$("#closeSummary").on('click', function(){ 
		showModal('confirm_logout', { 
			callback: function(){ nativeInterface.done(); },
			message: "Are you sure you want to close the summary? The account will be logged out when confirmed." 
		});
	});

	// EARN INPUT EVENTS
	$( "#earn.view #earnAmtW, #earn.view #earnAmtD" ).on( 'input focus blur keyup keydown', amountHandler );

}

function settingsHandler( e ) {
	if ( e.target != e.currentTarget ) {
		if ( e.target.getAttribute( 'data-sync' ) != null ) {
			nativeInterface.syncLocallyStoreData( e.target.getAttribute( 'data-sync' ) ); 
		} else {
			nativeInterface.settings();
		}
	}
}

function navigationHandler( e ) {
	if ( e.target != e.currentTarget && e.target.getAttribute( 'data-view' ) != null ) {

		// TEST
		// scanCallback('earn', '[{"profile": {"qrCard": "2ismeu832m3j3i", "email": "jericho@yahoo.com", "totalPoints": 1000, "dateOfBirth": "10/10/2016", "lastVisited":"Shangrila (Italiannis)", "lastTransaction":"2016-09-16 12:06:48"}}]');
		// offlineEarnCallback("2ismeu832m3j3i");

		// TEST
		// scanCallback('redeem', '[{"profile": { "newUser": "true", "totalSpent": "0", "qrCard": "2ismeu832m3j3i", "email": "jericho@yahoo.com", "totalPoints": "1000", 	"dateOfBirth": "10/10/2016", "lastVisited":"Shangrila (Italiannis)", "lastTransaction":"2016-09-16 12:06:48"}}]');

		// TEST
		// scanCallback('register', '2ismeu832m3j3i');

		if ( e.target.getAttribute( 'data-view' ) != 'convert' ) {
			
			nativeInterface.scan(e.target.getAttribute( 'data-view' ));

		} else {

			showConversion();

		}

	}
}

function scanCallback( type, data ) { // nativeInterface.scan callback
	if ( type == 'redeem' ) {

		showRedeemView(JSON.parse(data));

	} else if ( type == 'earn' ) {

		showEarnView(JSON.parse( data ));

	} else if ( type == 'register' ) {

		showRegistration( data );

	} else if ( type == 'renew' ) {

		fillOldCardInput( data );

	}
}

function getBadgeCount( type ) { // pointtable : registertable : renewtable
	
	// TEST
	// var count = 2;

	var count = nativeInterface.getEarnedCount( type );
	
	$( "#settings [data-sync='"+ type +"']" ).attr( 'data-rec', count ).toggle( parseInt(count) != 0 && !isNaN(count) );

}
// HOME END

// CONVERSION START
function showConversion() {
	var _navigate = function( e ) {
		if ( e.target != e.currentTarget ) {
			if ( e.currentTarget.parentElement.id == 'oldCardForm' ) {
				if ( $( e.target ).hasClass( 'next' ) ) {
					if ( $( "#oldCardForm input" ).val() == '' ) {
						nativeInterface.msgBox( 'Please enter or scan your old BFF card', '' );
					} else {
						nativeInterface.getProfile( $( "#oldCardForm input" ).val() );
						// getProfileCallback( JSON.stringify([{ profile : {} }]) );
					}
					
				} else if ( $( e.target ).hasClass( 'back' ) ) {
					closeRenewal();
				}
			}
		}
	},
	_scanOldCard = function() { nativeInterface.scan( 'renew' ) },
	_logOut = function() { 
		showModal('confirm_logout', { 
			callback: function(){ closeRenewal() },
			message: "Are you sure you want to log out?" 
		});
	}

	$( ".renew-box__nav" ).on( 'click', _navigate );
	$( ".renew-box__input-scan" ).on( 'click', _scanOldCard );
	// $( "#renew.view #logout" ).on( 'click', _logOut );
	$( "#renew.view #conversionForm #submit" ).on( 'click', function() { 
		showModal( 'confirm_convert', { 
			callback : function() {
				nativeInterface.convertPoints( $( "#oldCardForm input" ).val(), $( "#renew.view #points" ).text() );
			}
		});
	});

	$( "#renew.view" ).addClass( 'active-view' );
	$( "#oldCardForm" ).addClass( 'active-box' );
}

function closeRenewal() {
	$( "#renew.view" ).removeClass( 'active-view' );
	$( ".renew-box" ).removeClass( 'active-box' );
	$( "#renew.view input" ).val('').blur();

	$( "#renew.view #points" ).html( '' );
	$( "#renew.view #email" ).html( '' );
	$( "#renew.view #birthday" ).html( '' );
	$( "#renew.view #lastVisited" ).html( '' );
	$( "#renew.view #lastTransaction" ).html( '' );
	$( "#renew.view #code" ).html( '' );

	$( ".renew-box__nav, .renew-box__input-scan, #renew.view #logout, #conversionForm #submit" ).off( 'click' );
}

function getProfileCallback( data ) {
	var profile = JSON.parse( data )[0].profile;
	
	$( "#renew.view #points" ).html(profile.totalPoints);
	$( "#renew.view #email" ).html(profile.email);
	$( "#renew.view #birthday" ).html(profile.dateOfBirth);
	$( "#renew.view #lastVisited" ).html(profile.lastVisited);
	$( "#renew.view #lastTransaction" ).html(profile.lastTransaction);
	$( "#renew.view #code" ).html(profile.qrCard);

	$( "#oldCardForm" ).removeClass( 'active-box' );
	$( "#conversionForm" ).addClass( 'active-box' );
}

function fillOldCardInput( value ) { $( "#renew input" ).val( value ) } // under scanCallback for nativeinterface.scan('renew')

function convertPointsCallback() { 
	closeRenewal();
	closeModal();
	getBadgeCount( 'converttable' );
}

// CONVERT END

// REG START
function showRegistration(qrVal){
	var _submit = function(){
		var email = $("#register.view .form #email").val(),
			cardNum = $("#register.view .form #cardNum").val(),
			fname = $("#register.view .form #fname").val(),
			lname = $("#register.view .form #lname").val(),
			serverName = $("#register.view .form #serverName").val(),
			dopYear = $("#register.view .form #dop #year").val(),
			dopMonth = $("#register.view .form #dop #month").val(),
			dopDay = $("#register.view .form #dop #day").val(),
			dobYear = $("#register.view .form #dob #year").val(),
			dobMonth = $("#register.view .form #dob #month").val(),
			dobDay = $("#register.view .form #dob #day").val();

		if (cardNum == "") {
			$("#register.view .form #cardNum").focus();
			nativeInterface.msgBox("Please enter your card number!", "Empty Field!");
		} else if (email == "") {
			$("#register.view .form #email").focus();
			nativeInterface.msgBox("Please enter your email address!", "Empty Field!");
		} else if (!validateEmail(email)) {
			$("#register.view .form #email").focus();
			nativeInterface.msgBox("Please enter a valid email address!", "Invalid Input!");
		} else if (fname == "") {
			$("#register.view .form #fname").focus();
			nativeInterface.msgBox("Please enter your First Name!", "Empty Field!");
		} else if (lname == "") {
			$("#register.view .form #lname").focus();
			nativeInterface.msgBox("Please enter your Last Name!", "Empty Field!");
		} else if (serverName == "") {
			$("#register.view .form #serverName").focus();
			nativeInterface.msgBox("Please enter a the server's name!", "Invalid Input!");
		} else if (dopMonth == '' && dopDay == '' && dopYear == '') {
			nativeInterface.msgBox("Date of purchase is required!", "Empty Field!");
		} else if ((dopMonth != '' || dopDay != '' || dopYear != '') && !(dopMonth != '' && dopDay != '' && dopYear != '')) {
			nativeInterface.msgBox("Please complete your date of purchase!", "Invalid Input!");
		} else if (dobMonth == '' && dobDay == '' && dobYear == '') {
			nativeInterface.msgBox("Date of birth is required!", "Empty Field!");
		} else if ((dobMonth != '' || dobDay != '' || dobYear != '') && !(dobMonth != '' && dobDay != '' && dobYear != '')) {
			nativeInterface.msgBox("Please complete your date of birth!", "Invalid Input!");
		} else {
			var data = [
				{
					email: email,
					qrCard: cardNum,
					fname: fname,
					lname: lname,
					serverName: serverName,
					dateCardPurchased: dopYear+"-"+dopMonth+"-"+dopDay,
					dateOfBirth: dobYear+"-"+dobMonth+"-"+dobDay
				}
			]
			console.log(JSON.stringify(data));
			nativeInterface.register(JSON.stringify(data));
		}
	},
	_cancel = function(){
		// $("#register.view input").val('');
		// $("#register.view select").html('');
		// $("#register.view #submit").off('click');
		// $(this).off('click');
		// $(".active-view").removeClass('active-view');
		// $("#home.view").addClass('active-view');
		// getBadgeCount("registertable");

		showModal('confirm_logout', { 
			callback: function(){ nativeInterface.done() },
			message: "Are you sure you want to cancel your registration?" 
		});
	}

	$("#register.view #submit").on('click', _submit);
	$("#register.view #cancelReg").on('click', _cancel);

	$(".active-view").removeClass('active-view');
	$("#register.view").addClass('active-view');

	initializeDate('dob');
	initializeDate('dop');

	$("#register.view .form #cardNum").val(qrVal);
}

function registrationCallback(){
	$("#register.view input").val('');
	$("#register.view select").html('');
	$("#register.view #submit").off('click');
	$("#register.view #cancelReg").off('click');
	$(".active-view").removeClass('active-view');
	$("#home.view").addClass('active-view');
	getBadgeCount("registertable");
}

function initializeDate(parentId){
	var minYear = (parentId == "dop") ? (new Date().getFullYear() - 1) : 1940;
	var dateToday = new Date();
	var defaultMonth = (parentId == "dop") ? dateToday.getMonth()+1 : 12;
	var defaultDay = (parentId == "dop") ? dateToday.getDate() : daysInMonth(1, minYear);

	$("#register.view #"+parentId+" #year").html('<option value="" selected>Year</option>').off('change');
	$("#register.view #"+parentId+" #month").html('<option value="" selected>Month</option>').off('change');
	$("#register.view #"+parentId+" #day").html('<option value="" selected>Day</option>').off('change');

	for (i = new Date().getFullYear(); i >= minYear; i--){
		var currentYear = new Date();
	    $('#register.view #'+parentId+' #year').append($('<option />').val(i).html(i));   
	}

	for (i = 1; i <= defaultMonth; i++){
		var mon = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept ","Oct","Nov","Dec"];
	    $('#register.view #'+parentId+' #month').append($('<option />').val(i).html(mon[i-1]));
	}
	    
	for(i=1; i <= defaultDay; i++){
        $('#register.view #'+parentId+' #day').append($('<option />').val(i).html(i));
    }

    if (parentId == "dop") {
    	$("#register.view #"+parentId+" #year").val(dateToday.getFullYear());
    	$("#register.view #"+parentId+" #month").val(dateToday.getMonth()+1)
    	$("#register.view #"+parentId+" #day").val(dateToday.getDate())
    }
    $('#register.view #'+parentId+' #year, #register.view #'+parentId+' #month').on('change', updateNumberOfDays);
}

function updateNumberOfDays(e){
	var parent = $(e.target).parent()[0].id;
	var month = $('#register.view #'+parent+' #month').val();
	var year = $('#register.view #'+parent+' #year').val();
	var day = $('#register.view #'+parent+' #day').val();
	var days = daysInMonth(month, year);

	$('#register.view #'+parent+' #day').html('<option value="">Day</option>');
	
    for(i=1; i < days+1 ; i++){
    	if((day!='' && month!='') && (((days < day) && (i == days)) || i==day)){
            $('#register.view #'+parent+' #day').append($('<option selected />').val(i).html(i));
        }else{
            $('#register.view #'+parent+' #day').append($('<option />').val(i).html(i));
        }
    }


    if (typeof e != "undefined" && e.target.id == "year" ) {
    	var monthToday = new Date().getMonth();
		var mon = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept ","Oct","Nov","Dec"];
		
		$('#register.view #'+parent+' #month').html('<option value="">Month</option>');
		for (i = 1; i <= ((year == new Date().getFullYear()) ? monthToday+1 : 12); i++){
		    $('#register.view #'+parent+' #month').append($('<option />').val(i).html(mon[i-1]));
		}
    }
}

function daysInMonth(month, year) {
	var d = new Date(year, month, 0);
    var current = new Date(new Date().getFullYear(), new Date().getMonth(), 0);
	if (d > current) {
		return new Date().getDate();
	}else{
    	return d.getDate();
	}
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}
// REG END

// EARN START
function offlineEarnCallback(qrcode){ showEarnView("", qrcode) }
function showEarnView(data, qrcode){
	var _submit = function(){
		var numbersOnly = /^\d+$/;

		if ($("#earn.view #transID").val() == ""){
			$("#earn.view #transID").focus();
			nativeInterface.msgBox("Please enter the transaction ID", "Oops!");
		} else if ( !numbersOnly.test($("#earn.view #earnAmtD").val()) ) {
			$("#earn.view #earnAmtD").focus();
			nativeInterface.msgBox('Invalid input!', 'Oops!');
		} else if ( !numbersOnly.test($("#earn.view #earnAmtW").val()) ) {
			$("#earn.view #earnAmtW").focus();
			nativeInterface.msgBox('Invalid input!', 'Oops!');
		} else if ( parseInt($("#earn.view #earnAmtW").val()) == 0) {
			$("#earn.view #earnAmtW").focus();
			nativeInterface.msgBox("Please enter the earn amount", "Oops!");
		} else if ($("#earn.view #earnAmtW").val().length > 7) {
			$("#earn.view #earnAmtW").focus();
			nativeInterface.msgBox("Input limit exeeded!", "Oops!");
		} else {
		 	$("input").blur();
			var earnData = {
				contentData: [{
					transID: $("#earn.view #transID").val(),
					amount: $("#earn.view #earnAmtW").val() +"."+ $("#earn.view #earnAmtD").val()
				}],
				callback: function(){
					// console.log(JSON.stringify(earnData.contentData))
					// nativeInterface for earning
					nativeInterface.earn(JSON.stringify(earnData.contentData));
				}
			}
			showModal('confirm_earn', earnData)
		}
	}

	$( "#earn.view #earnAmtW" ).val( '0' );
	$( "#earn.view #earnAmtD" ).val( '00' );

	if (data != "") {
		var profile = data[0].profile;
		$("#earn.view #points").html(profile.totalPoints).show();
		$("#earn.view #birthday").html(profile.dateOfBirth).show();
		$("#earn.view #lastVisited").html(profile.lastVisited).show();
		$("#earn.view #lastTransaction").html(profile.lastTransaction).show();
		$("#earn.view #email").html(profile.email);
		$("#earn.view #code").html(profile.qrCard);
	}else{
		$("#earn.view #points, #earn.view #birthday, #earn.view #lastVisited, #earn.view #lastTransaction").html("").hide();
		$("#earn.view #email").html("");
		$("#earn.view #code").html(qrcode);
	}

	$("#earn.view #submit").on('click', _submit);

	$(".active-view").removeClass('active-view'); 
	$("#earn.view").addClass('active-view');
}

/**
 * EARN INPUT HANDLERS
 */
function amountHandler( e ) {
	if ( this.id == 'earnAmtD' ) {
		if ( e.type == 'focus' ) {
			if ( this.value == '00' ) {
            	this.value = '';
          	}
		} else if ( e.type == 'blur' ) {
			if(this.value == '' || this.value == '00') {
				this.value = '00';
			} else if (this.value.length == 1) {
				this.value += "0";
			}
		} else if ( e.type == 'input' ) {
			if (this.value.length > 2) {
            	this.value = this.value.split('').splice(0, 2).join('');
          	}
		}
	} else if ( this.id == 'earnAmtW' ) {
		if ( e.type == 'focus' ) {
			if ( this.value == '0' ) {
            	this.value = '';
          	}
		} else if ( e.type == 'blur' ) {
			if(this.value == '' || this.value == '0') {
				this.value = '0';
			}
		} else if ( e.type == 'keyup' || e.type == 'keydown' ) {
			if ( this.value.length == 7
		        && e.keyCode != 46 // delete
		        && e.keyCode != 8 // backspace
		       ) {
				e.preventDefault();
			}
		}
	}
}

// EARN END

// REDEEM START
function showRedeemView(data){
	var profile = data[0].profile,
		isNewUser = ( typeof profile.newUser != 'undefined' && profile.newUser == 'true' ),
		_numberWithCommas = function ( x ) { return x.replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
		_submit = function() {
		var activeVouchers = $("#redeem.view #vouchers .active-voucher");
		var selectedVouchers = [];
		var numbersOnly = /^\d+$/;

		for (var i = 0; i < activeVouchers.length; i++) {
			var arr = [];
			arr.push(GLOBAL_VOUCHER_LIST[activeVouchers[i].id.split("_")[1]].voucherID)
			arr.push(GLOBAL_VOUCHER_LIST[activeVouchers[i].id.split("_")[1]].name)
			selectedVouchers.push(arr);
		}
		
		$("input").blur();
		if (!numbersOnly.test($("#redeem.view #pointsToRedeem").val()) && $("#redeem.view #pointsToRedeem").val() != "") {
			$("#redeem.view #pointsToRedeem").focus();
			nativeInterface.msgBox('Invalid input!', 'Oops!');
		} else if (parseInt($("#pointsToRedeem").val()) > parseInt(profile.totalPoints)) {
			$("#redeem.view #pointsToRedeem").focus();
			nativeInterface.msgBox('Insufficient Points!', 'Oops!')
		} else if ($("#pointsToRedeem").val() == "" && selectedVouchers.length == 0) {
			nativeInterface.msgBox('Please pick something to redeem', 'Oops!');
		}else{
			var completeData = [{
				points: ($("#pointsToRedeem").val() == "") ? 0 : parseInt($("#pointsToRedeem").val()),
				vouchers: selectedVouchers
			}, {
				callback: function(){
					var vouchers = [];
					for (var i = 0; i < completeData[0].vouchers.length; i++) {
						vouchers.push(completeData[0].vouchers[i][0]);
					}
					var redeemData = [{
						points: completeData[0].points,
						vouchers: vouchers
					}]
					// console.log(redeemData);
					// nativeInterface for redeem
					nativeInterface.redeem(JSON.stringify(redeemData))
					// redeemCallback(JSON.stringify(redeemData));
				}
			}];
			showModal('confirm_redeem', completeData);
		}

		
	}

	GLOBAL_MEMBER_ID = profile.memberID;

	$("#redeem.view #points").html( _numberWithCommas((( isNewUser ) ? profile.totalSpent : profile.totalPoints ).toString()) )
							 .toggleClass( 'spent', ( isNewUser ));
	$("#redeem.view #email").html( profile.email );
	$("#redeem.view #birthday").html(profile.dateOfBirth);
	$("#redeem.view #lastVisited").html(profile.lastVisited);
	$("#redeem.view #lastTransaction").html(profile.lastTransaction);
	$("#redeem.view #code").html(profile.qrCard);

	nativeInterface.getVoucherList(GLOBAL_MEMBER_ID, false);
	// vouchersCallback()

	$("#redeem.view #submit").on('click', _submit);

	$(".active-view").removeClass('active-view'); 
	$("#redeem.view").addClass('active-view');
}

function vouchersCallback(data){
	GLOBAL_VOUCHER_LIST = JSON.parse(data);
	
	// TEST DATA
	// GLOBAL_VOUCHER_LIST = [{
	// 	"id": 271,
	// 	"voucherID": "VCH001RNA892RTN",
	// 	"memberID": "MEMTT142336DlHic",
	// 	"email": "facejun@yahoo.com",
	// 	"name": "20% OFF YOUR BILL from TUESDAY to SUNDAY",
	// 	"image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/S1G0201608231455385i56.png",
	// 	"startDate": "2016-09-06",
	// 	"endDate": "2017-09-06",
	// 	"type": "discounts",
	// 	"cardType": "BFF",
	// 	"cardVersion": "BFF2016",
	// 	"frequencyType": "day",
	// 	"frequencyStart": "1",
	// 	"frequencyEnd": "6",
	// 	"parameterString": "DISCPRC",
	// 	"parameterValue": "20",
	// 	"action": "COPY",
	// 	"status": "active",
	// 	"dateAdded": "2016-09-06 14:10:36",
	// 	"dateModified": "2016-09-06 14:10:36",
	// 	"count": 1,
	// 	"weekDay": 1,
	// 	"description": "20% OFF YOUR BILL from TUESDAY to SUNDAY",
	// 	"terms": "Card holder is entitled to 20% OFF on  purchase of F &amp; B in participating Bistro Group restaurants from Tuesdays to Sundays\n\n30% OFF your bill every Monday\nComplimentary single serving of regular coffee or hot tea with entreepurchase. Exclusive for card holder only.\n\nBistro Frequent Foodie Discounts can be combined with the Bistro Frequent Foodie vouchers \nBistro Frequent Foodie Discounts are not to be combined with other discounts and promotions, senior citizen\/PWD discounts, party packages\nNot valid for purchase of gift certificates\nNot valid on Mother&rsquo;s day, Father&rsquo;s day and Valentine&rsquo;s Day. \n\nNo card, no discount\nDiscount can be availed right after purchase up to a total discount value of P1,500 on initial card use\nBFF  vouchers can be used on the next visit \nThe Bistro Group reserves the right of final interpretation of all terms and conditions of use, which are subject to change without prior notice"
	// }, {
	// 	"id": 266,
	// 	"voucherID": "VCH002RNB814RXL",
	// 	"memberID": "MEMTT142336DlHic",
	// 	"email": "facejun@yahoo.com",
	// 	"name": "P250.00 OFF E-VOUCHERS",
	// 	"image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/iEU920160823145608vELC.png",
	// 	"startDate": "2016-09-06",
	// 	"endDate": "2017-09-06",
	// 	"type": "benefits",
	// 	"cardType": "BFF",
	// 	"cardVersion": "BFF2016",
	// 	"frequencyType": "day",
	// 	"frequencyStart": "0",
	// 	"frequencyEnd": "6",
	// 	"parameterString": "DISCAMT",
	// 	"parameterValue": "250",
	// 	"action": "MOVE",
	// 	"status": "active",
	// 	"dateAdded": "2016-09-06 14:10:36",
	// 	"dateModified": "2016-09-06 14:10:36",
	// 	"count": 4,
	// 	"weekDay": 1,
	// 	"description": "P250.00 OFF E-VOUCHERS",
	// 	"terms": "The P250 off voucher can be used on the next visit after The BFF Card purchase\n\nEnjoy the P250 off voucher with a minimum purchase of P1,000. One voucher per visit only\n\nThe P250 off voucher can be combined with the Bistro Frequent Foodie discounts\n\nNot valid on Mother&rsquo;s day, Father&rsquo;s day and Valentine&rsquo;s Day\n\nVoucher is non-transferable. Voucher cannot be converted to cash\n\nCannot be combined with other promotions and discounts except the Bistro Premiere Card discount privileges\n\nThe Bistro Group reserves the right to final interpretation of all terms and conditions of which are subject to change without prior notice\n\n The voucher is valid for one year from purchase date. Same validity with BFF Card\n"
	// }, {
	// 	"id": 273,
	// 	"voucherID": "VCHSq32054mkr4T6",
	// 	"memberID": "MEMTT142336DlHic",
	// 	"email": "facejun@yahoo.com",
	// 	"name": "Complimentary Regular Coffee or Hot Tea",
	// 	"image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/AFBt201608241540126Iem.png",
	// 	"startDate": "2016-09-06",
	// 	"endDate": "2017-09-06",
	// 	"type": "discounts",
	// 	"cardType": "BFF",
	// 	"cardVersion": "BFF2016",
	// 	"frequencyType": "day",
	// 	"frequencyStart": "0",
	// 	"frequencyEnd": "6",
	// 	"parameterString": "DISCPRC",
	// 	"parameterValue": "100",
	// 	"action": "COPY",
	// 	"status": "active",
	// 	"dateAdded": "2016-09-06 14:10:36",
	// 	"dateModified": "2016-09-06 14:10:36",
	// 	"count": 1,
	// 	"weekDay": 1,
	// 	"description": "Complimentary Regular Coffee or Hot Tea",
	// 	"terms": "Get 350 Less on Bulgogi Borthers"
	// }, {
	// 	"id": 270,
	// 	"voucherID": "VCHu2152347yuGMG",
	// 	"memberID": "MEMTT142336DlHic",
	// 	"email": "facejun@yahoo.com",
	// 	"name": "P1000 OFF BIRTHDAY E-VOUCHER",
	// 	"image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/4WgF201608241522471VWH.png",
	// 	"startDate": "2016-09-06",
	// 	"endDate": "2017-09-06",
	// 	"type": "birthday",
	// 	"cardType": "BFF",
	// 	"cardVersion": "BFF2016",
	// 	"frequencyType": "month",
	// 	"frequencyStart": "0",
	// 	"frequencyEnd": "6",
	// 	"parameterString": "DISCAMT",
	// 	"parameterValue": "1000",
	// 	"action": "MOVE",
	// 	"status": "active",
	// 	"dateAdded": "2016-09-06 14:10:36",
	// 	"dateModified": "2016-09-06 14:10:36",
	// 	"count": 1,
	// 	"weekDay": 1,
	// 	"description": "P1000 OFF BIRTHDAY E-VOUCHER",
	// 	"terms": "The P1000 off birthday voucher should be claimed on your birth month\n\nGet P1000 off your total bill with a minimum purchase of P3,000\n\n Present the Bistro Frequent Foodie Card and I.D. bearing your birthday to avail of the P1000 off Birthday voucher\nP1000 off birthday voucher can only be used once.\n\nThe P1000 off birthday voucher can be combined with the Bistro Frequent Foodie card discount\n\nVoucher is non-transferable. Voucher cannot be converted to cash\n\nCannot be combined with the P250 off voucher , other promotions and discounts except the BFF Card discount\n\nNot valid on Mother&rsquo;s day, Father&rsquo;s day, and Valentine&rsquo;s day\n\nThe Bistro Group reserves the right to final interpretation of all terms and conditions of which are subject to change without prior notice\n"
	// }, {
	// 	"id": 274,
	// 	"voucherID": "VCHUf212926p8hPa",
	// 	"memberID": "MEMTT142336DlHic",
	// 	"email": "facejun@yahoo.com",
	// 	"name": "We Miss You",
	// 	"image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/695V20160824214826ZP5c.png",
	// 	"startDate": "2016-09-06",
	// 	"endDate": "2016-10-06",
	// 	"type": "wemissyou",
	// 	"cardType": "BFF",
	// 	"cardVersion": "BFF2016",
	// 	"frequencyType": "year",
	// 	"frequencyStart": "0",
	// 	"frequencyEnd": "6",
	// 	"parameterString": "DISCAMT",
	// 	"parameterValue": "500",
	// 	"action": "MOVE",
	// 	"status": "active",
	// 	"dateAdded": "2016-09-06 14:19:19",
	// 	"dateModified": "2016-09-06 14:19:19",
	// 	"count": 1,
	// 	"weekDay": 1,
	// 	"description": "We Miss You Very Much",
	// 	"terms": "We Miss You Very Much"
	// }]
	// GLOBAL_VOUCHER_LIST = [];

	var _syncVouchers = function(){ 
		$("#redeem.view #vouchers").html('<div class="loading-vouchers">Getting your vouchers...</div>');
		nativeInterface.getVoucherList(GLOBAL_MEMBER_ID, false);
	},
	_toggleVoucherSelection = function(e){ // TAG SELECTED VOUCHERS VIA CLASS. USE CLASS TO GET SELECTED ELEMENTS. 
		if (e.target != e.currentTarget) {
			$(e.target).toggleClass('active-voucher');
		}
	},
	_showPopupVoucher = function(index){
		$("#voucherModal .active-box").removeClass('active-box')
		if (typeof index == "undefined") {
			$($("#voucherModal").find(".voucher-modal-box")[0]).addClass('active-box');
		}else{
			if (index == popup.length) {
				$("#voucherModal").html("").hide();
			}else{
				$($("#voucherModal").find(".voucher-modal-box")[index]).addClass('active-box');
			}
		}
	}, 
	_formatDate = function( date ) {
		var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			dateArr = date.split('-');
		return months[ parseInt(dateArr[1]) - 1 ] +" "+ dateArr[2] +", "+ dateArr[0];
	}

	if (GLOBAL_VOUCHER_LIST.length != 0) {
		var voucherElems = "";
		var popup = [];
		var popupText = {
			"wemissyou": "Welcome back BFF Cardholder! Since we miss you, please enjoy this voucher, on us. Thank you",
			"birthday": "Happy Birthday! Enjoy this voucher, on us. Thank you"
		}

		// SET VOUCHERS IN A GRID WHILE CHECKING FOR BIRTHDAY AND WEMISSYOU VOUCHERS
		$("#redeem.view #vouchers").html('').off('click');
		for (var i = 0; i < GLOBAL_VOUCHER_LIST.length; i++) {

			voucherElems += '<div id="voucher_'+i+'" class="voucher" >';
				voucherElems += '<div class="voucher__count">'+ GLOBAL_VOUCHER_LIST[i].count +'</div>';
				voucherElems += GLOBAL_VOUCHER_LIST[i].name;
				voucherElems += '<div class="voucher__expiration">'+ _formatDate( GLOBAL_VOUCHER_LIST[i].endDate ) +'</div>';
			voucherElems += '</div>';

			if (GLOBAL_VOUCHER_LIST[i].type == "wemissyou" || GLOBAL_VOUCHER_LIST[i].type == "birthday") {
				popup.push({ txt : popupText[GLOBAL_VOUCHER_LIST[i].type], img :  GLOBAL_VOUCHER_LIST[i].image });
			};
		}
		$("#redeem.view #vouchers").html(voucherElems).on('click', _toggleVoucherSelection);

		// IF BIRTHDAY AND/OR WEMISSYOU VOUCHER IS AVAILABLE, THEN, SET POPUP IMAGES
		if (popup.length != 0) {
			var currentPopup = 0;
			var popupContent = "";

			$("input").blur();
			for ( var i = 0; i < popup.length; i++ ) {
				popupContent += '<div class="voucher-modal-box">';
					popupContent += '<div class="voucher-modal__content">';
						popupContent += '<div class="voucher-modal__title">'+ popup[i].txt +'</div>';
						popupContent += '<div class="voucher-modal__img" style="background-image: url('+ popup[i].img +')"></div>';
					popupContent += '</div>';
					popupContent += '<div class="voucher-modal-close">Ok</div>';
				popupContent += '</div>';

			}
			$("#voucherModal").html(popupContent).show();
			$("#voucherModal .voucher-modal-close").on('click', function(){
				currentPopup += 1;
				_showPopupVoucher(currentPopup)
			});
			_showPopupVoucher()
		}

	}else{
		$("#redeem.view #vouchers .empty-vouchers").off('click');
		$("#redeem.view #vouchers").html('<div class="empty-vouchers"></div>');
		$("#redeem.view #vouchers .empty-vouchers").on('click', _syncVouchers);
	}
}

function errorCallback(){
	$("#redeem.view #vouchers .empty-vouchers").off('click');
	$("#redeem.view #vouchers").html('<div class="empty-vouchers"></div>');
	$("#redeem.view #vouchers .empty-vouchers").on('click', function(){
		$("#redeem.view #vouchers").html('<div class="loading-vouchers">Getting your vouchers...</div>');
		nativeInterface.getVoucherList(GLOBAL_MEMBER_ID, false);
	});
}

function redeemCallback(data){
	var summary = JSON.parse(data);


	$("#modal .modal-body").html('');
	$("#modal .modal-options .btn").off('click');
	$("#modal").hide();

	if (summary.length != 0) {
		$("#redeemSummary.view .redeem-summary-points").html(((summary[0].points == "") ? 0 : summary[0].points));
		if (summary[0].successVoucher.length != 0 || summary[0].errorVoucher.length != 0) {
			var voucherSummaryList = "<ul>";
			var errorVoucherList = "<ul class='error-vouchers'>";

			for (var i = 0; i < summary[0].successVoucher.length; i++) {
				for (var x = 0; x < GLOBAL_VOUCHER_LIST.length; x++) {
					if (GLOBAL_VOUCHER_LIST[x].voucherID == summary[0].successVoucher[i]) {
						voucherSummaryList += "<li>"+GLOBAL_VOUCHER_LIST[x].name+"</li>";
					}else if(GLOBAL_VOUCHER_LIST[x].voucherID == summary[0].errorVoucher[i]){
						errorVoucherList += "<li>"+GLOBAL_VOUCHER_LIST[x].name+"</li>";
					}

				}
			}

			voucherSummaryList += "</ul>";
			errorVoucherList += "</ul>";
			$("#redeemSummary.view .redeem-summary-vouchers").html(voucherSummaryList+((summary[0].errorVoucher.length != 0) ? errorVoucherList : ""));
		}else{
			$("#redeemSummary.view .redeem-summary-vouchers").html("<div>No Vouchers Redeemed</div>");
		}

		$(".active-view").removeClass('active-view');
		$("#redeemSummary.view").addClass('active-view');
	}else{
		$(".active-view").removeClass('active-view');
		$("#redeem.view").addClass('active-view');
		nativeInterface.msgBox('We could not redeem your rewards. Please try again!', 'Oops!');
	}
}
// REDEEM END

// EARN & REDEEM GENERAL FUNCTIONS
function logout(){
	GLOBAL_MEMBER_ID = "";
	GLOBAL_VOUCHER_LIST = [];

	$("input").val('').blur();

	$("#earn.view #email").html("");
	$("#earn.view #code").html("");
	$("#earn.view #submit").off('click');

	getBadgeCount("pointtable");

	$("#redeem.view #points").html("");
	$("#redeem.view #email").html("");
	$("#redeem.view #code").html("");
	$("#redeem.view #vouchers .empty-vouchers").off('click');
	$("#redeem.view #vouchers").html("").off('click');

	$("#modal .modal-body").html('');
	$("#modal .modal-options .btn").off('click');
	$("#modal").hide();

	$(".active-view").removeClass('active-view');
	$("#home.view").addClass('active-view');
}

function showModal(type, data){

	switch(type){
		case 'confirm_earn':
			$("#modal .modal-body").html("<div class='modal-earn-msg'>Are you sure you want to continue with the following earn details?</div>"+
										 "<div class='modal-earn-transID'>Transaction ID: "+data.contentData[0].transID+"</div>"+
										 "<div class='modal-earn-amount'>Earn Amount: "+data.contentData[0].amount+"</div>");
			$("#modal .modal-options #confirm.btn").on('click', data.callback);
			$("#modal .modal-options #cancel.btn").on('click', closeModal);
			$("#modal").show();
		break;
		case 'confirm_redeem':
			var vouchers = "none";
			if (data[0].vouchers.length != 0) {
				vouchers = "<ul>";
				for (var i = 0; i < data[0].vouchers.length; i++) {
					vouchers += "<li>"+data[0].vouchers[i][1]+"</li>";
				}
				vouchers += "</ul>";
			}

			$("#modal .modal-body").html("<div class='modal-redeem-msg'>The following items will be deducted from your account: </div>"+
										 "<div class='modal-redeem-points'>"+data[0].points+"</div>"+
										 "<div class='modal-redeem-vouchers'>"+vouchers+"</div>");
			$("#modal .modal-options #confirm.btn").on('click', data[1].callback);
			$("#modal .modal-options #cancel.btn").on('click', closeModal);
			$("#modal").show();
		break;
		case 'confirm_logout':
			$("#modal .modal-box").attr("style", "padding: 40px 10px 90px; height:auto;");
			$("#modal .modal-body").html("<div class='modal-earn-msg' >"+data.message+"</div>");
			$("#modal .modal-options #confirm.btn").on('click', data.callback);
			$("#modal .modal-options #cancel.btn").on('click', closeModal);
			$("#modal").show();
		break;
		case 'confirm_convert':
			$("#modal .modal-box").attr("style", "padding: 40px 10px 90px; height:auto;");
			$("#modal .modal-body").html("<div class='modal-earn-msg'>Are you sure you want to convert all your points?</div>");
			$("#modal .modal-options #confirm.btn").on('click', data.callback);
			$("#modal .modal-options #cancel.btn").on('click', closeModal);
			$("#modal").show();
		break;
		default:
		break;
	}
}

function closeModal() {
	$("#modal .modal-box").attr("style", "");
	$("#modal .modal-body").html('');
	$("#modal .modal-options .btn").off('click');
	$("#modal").hide();
}